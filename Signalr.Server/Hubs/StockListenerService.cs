using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using SignalR.Server;

public class StockListenerService : BackgroundService
{
    private readonly IHubContext<GreeterHub, IGreeterHubClient> _hubContext;
    private readonly List<string> stockNames = new List<string>() { "MSFT", "AAPL", "TSLA" };
    private readonly List<decimal> stockValues = new List<decimal> { 300, 150, 2000 };

    public StockListenerService(IHubContext<GreeterHub, IGreeterHubClient> hubContext)
    {
        _hubContext = hubContext;
    }

    public async Task OnStockUpdated()
    {
        var index = new Random().Next(0, 3);

        stockValues[index] += new Random().Next(-10, 10);

        var clientGroup = _hubContext.Clients.Group(stockNames[index]);
        await clientGroup.ReceiveUpdate(stockNames[index], stockValues[index]);
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await OnStockUpdated();
            await Task.Delay(500, stoppingToken);
        }
    }
}